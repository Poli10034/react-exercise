import React, { useState, useEffect } from 'react';
import axios from 'axios'; 
import Project from './Project';
import Form from './Form';
import { withStyles } from '@material-ui/core/styles';


import { Button, List, Grid} from '@material-ui/core';

const styles ={
   
  blueCard:{
    width: '330px',
    height: '340px',
    background: '#111160',
    padding: '1% 2% 2% ',
    borderRadius: 5,
    display: 'block',
    maxHeight: '200',
    overflow: 'auto'
  },
  imgPaper: {
    height: '300px',
    width: '250px',
    display: 'block',
    margin: '-140px 30px 0 60px',
    backgroundImage: `url(${"https://www.indonesia.travel/content/dam/indtravelrevamp/en/destinations/point-of-interest/cendrawasih-the-bird-of-paradise/cendrawasih%201.jpg"})`,
    backgroundPosition: 'center',
    border: 0,
    borderRadius: 5,
    'background-size': 'cover'
  }
}

 function ProjectsList(props){
   const {classes} = props;
    const [visiableNum, setVisiableNum] = useState(5);
    const [projects, setProjects] = useState([]);
    const [ edit, setEditing] = useState(false)   //To re-fetch the data from the WS, but in this case the WS data not actually changing

    const apiUrl = "https://jsonplaceholder.typicode.com/posts/";

    useEffect(() => {
        async function fetchData(){
            var data =[];
            try{
                data = await fetch(apiUrl)
                .then(response => response.json())
            }catch(err){
                console.log('error fetching...:', err);
            }
            const loadProjects = data.map(item=> ({id: item.id, title: item.title, body: item.body }));
            setEditing(false);
            return setProjects(loadProjects);
        }
        fetchData();
    },[edit]);

    const updateProject = (newProj, i) =>{
      try{
        fetch(apiUrl + i, {
            method: 'PUT',
            body: JSON.stringify({
              id: i,
              title: newProj,
              body: 'defualt data',
              userId: 1
            }),
            headers: {
              "Content-type": "application/json; charset=UTF-8"
            }
          })
          .then(response => response.json())
          .then(json => console.log(json))
        // console.log(apiUrl + i);
        // axios.patch(apiUrl + i, {title: newProj})
        // .then(response => console.log(response) );

        }catch(err){
          console.log('error fetching...:', err);
        }
        // setEditing(true);
        setProjects(prevState=> prevState.map(data => data.id!== i ? data : { ...data, title: newProj }));
        
    };

    const deleteProject = (id) => {
        console.log(`deleted: ${id}`);
        try{
        axios.delete(apiUrl + id)
        .then(response => console.log(response) );
        }catch(err){
          console.log('error fetching...:', err);
        }
        // setEditing(true);
        setProjects(prevState => prevState.filter(project => project.id !== id));
      };


      const loadMore = () => {
          setVisiableNum(prevState => prevState + 7);
          setProjects(prevState=> prevState.map(item=> ({id: item.id, title: item.title, body: item.body })));
      };

      const addProject = ({ name, publisher, desc, id = null, date }) => { 
        console.log(name, desc, desc, date);

        try{
          fetch(apiUrl, {
            method: 'POST',
            body: JSON.stringify({
              title: name,
              body: publisher + desc,
              userId: 1
            }),
            headers: {
              "Content-type": "application/json; charset=UTF-8"
            }
          })
          .then(response => response.json())
          .then(json => console.log(json))
        }catch(err){
          console.log('error fetching...:', err);
        }

        setProjects(prevState => ([
           {
              id: projects.length + 1,
              title: name,
              body: desc,
              publisher: publisher,
              date: date
            },...prevState])
        )
      };

      const renderEachProject = (item, i) => {
        return (
          <div
            key={ `container${item.id}` }
            className="card"
            style={ { width: '18rem', marginBottom: '0px' } }
          >
            <div className="card-body">
              <Project
                index={ item.id }
                onChange={ updateProject }
                onDelete={ deleteProject }
              >
               
                <h5 className="card-title">{ item.title }</h5>
              </Project>
            </div>
          </div>
        );
      };

      return (
          
        <Grid container   className="ProjectsList"  direction="row"
        justify="space-evenly">
        
          
          <Grid  >
            <Form onSubmit={ addProject }/>
          </Grid>
          <Grid  className={classes.imgPaper}>
            
          </Grid>
          <Grid  >
          <List  className={classes.blueCard} style={{maxHeight: '100%', overflow: 'auto'}}>
            { projects.map(renderEachProject).slice(0 , visiableNum ) }
          </List>
          <Button id="showMoreBtn" variant="text" onClick={loadMore}>Show More</Button>  
          </Grid>     
        </Grid>
      );
}

export default withStyles(styles)(ProjectsList);