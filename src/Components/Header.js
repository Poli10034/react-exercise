import React from 'react';
import {Typography, Toolbar} from '@material-ui/core';


const Header = () => {
return (
        <Toolbar alignitems="center" style={{width: '100%'}}>
            <Typography alignitems="center" variant="h4" color="initial" style={{fontSize: '26px', margin: 'auto'}}>
                Project Name
            </Typography>
        </Toolbar>
);
};

export default Header;