import React, { useState } from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import {TextField, Button, IconButton, ListItemSecondaryAction, ListItemText, ListItem} from '@material-ui/core';

export default function Project(props){
    const{
        onDelete,
        index,
        children,
        onChange,
    } = props;
    const [editing, setEditing] = useState(false);
    const [name, setName] = useState('');

    const editProject =() => setEditing(true);
    const deleteProject = () => onDelete(index);
    const saveProject = (event) => {
      event.preventDefault();
      console.log(name, index);
      onChange(name, index);
      setEditing(false);
    };

    function Form() {
        return (
          <div>
            <form>
            <TextField
                    className="textField"
                    variant="outlined"
                    size="small"
                    placeholder="Project Name"
                    margin="normal"
                    value={name} 
                    onChange={(event) => {
                        console.log(event.target.value);
                        event.preventDefault();
                        setName(event.target.value);
                        }}    
                        
                />
                <Button onClick={saveProject} > <CheckCircleIcon className="save" /> </Button>
            </form>
          </div>
        );
      }
    
      function UI() {
        return (
         <div className="project">

            <ListItem>
                <ListItemText >
                    <div >{children}</div>
                 </ListItemText>
                 <ListItemSecondaryAction style={{ right: '-6%'}}>
                     <IconButton onClick={editProject}>
                     <EditIcon style={ {color: 'white' }}  />
                     </IconButton>
                    <IconButton onClick={deleteProject} edge="end" aria-label="delete">
                      <DeleteIcon style={ {color: 'rgba(148,187,233,1)' }} />
                    </IconButton>
                  </ListItemSecondaryAction>
            </ListItem >
            </div>
            );

      }
    
      return editing ? <Form /> : <UI />;
}