import React,  { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import {TextField, Button, Grid} from '@material-ui/core';
// import TextField from '@material-ui/core/TextField';


const styles = {
    containerCard: {
        
        height: '100%',
        width: '100%',
        borderRadius: 5,
        display: 'initial',
    },
    blueCard:{
        width: '300px',
        background: '#111160',
        padding: '7% 0 20%',
        borderRadius: 5,
        display: 'block',
       
    },
    textField: {
        height: '40px',
        background: 'white',
        borderRadius: 5,
        margin: '0px 0px 7px 15%'
    },
    bigField: {
        background: 'white',
        borderRadius: 5,
        margin: '0px 0px 7px 15%',

    }
}
const FormCard = (props, {addProject}) =>{
const {onSubmit ,classes} = props;
const [name, setName] = useState('');
const [publisher, setPName] = useState('');
const [date, setDate] = useState('');
const [desc, setdesc] = useState('');

const addNewProj = (event) => {
    event.preventDefault();
    console.log(name);
    onSubmit({name, publisher, desc, date});
};
    return (
        <Grid container className={classes.containerCard}   alignItems="center"
        direction="column"
        >
        <Grid
        container 
        className={classes.blueCard}
        alignItems="center"
        direction="column"
        justify="space-evenly"
        
        >
            <form style={{width: '300px'}} 
        //     onSubmit={(event) => {
        //     event.preventDefault();
        //     console.log(`FInal data: ${name}` );
        //     addProject(name, desc);
        //   }}
          >
            <TextField
                    className={classes.textField}
                    variant="outlined"
                    size="small"
                    placeholder="Project Name"
                    margin="normal"
                    value={name}
                    onChange={(event) => {
                    setName(event.target.value);
                console.log(name) ;                  }}
                   
                />
                 <TextField
                 className={classes.textField}
                    variant="outlined"
                    size="small"
                    placeholder="Publisher Name"
                    margin="normal"
                    value={publisher}
                    onChange={(event) => {
                    setPName(event.target.value);
                    console.log(event.target.value) ;  }}

                />
                 <TextField
                 className={classes.textField}
                    variant="outlined"
                    size="small"
                    placeholder="Date"
                    margin="normal"
                    onChange={(event) => {
                    setDate(event.target.value);
                    }}
                    value={date}
                />
           
                <TextField  id="outlined-textarea" className={classes.bigField} variant="outlined"
                    multiline
                    rows="4"
                    rowsMax={4}
                    placeholder="Description"
                    margin="normal"
                    onChange={(event) => {
                    setdesc(event.target.value);
                    }}
                    value={desc} />
                    

         </form>
        
        </Grid>
        <Button
                            id="add"
                            type="submit"
                            onClick= { addNewProj }
                >
                <AddCircleIcon style={{ fontSize: 50 }} />
            </Button>
        </Grid>
            
        
      );
}
export default withStyles(styles)(FormCard);

