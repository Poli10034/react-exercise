
import React  from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Header from './Components/Header';
import ProjectList from './Components/ProjectList';
import './App.css'

import { Container, Grid } from '@material-ui/core';

const styles = {
 root: {
  background: 'radial-gradient(circle, rgba(238,174,202,1) 2%, rgba(148,187,233,1) 90%)',
  minHeight: '100vh',
   maxWidth: '100%',
   color: 'white',
   padding: '5% 8% 3% 8%'
  
 },
 content: {
   minHeight: '50vh',
   'border-radius': '5px',
   backgroundSize: 'cover',

 },
 paperContainer: {
  height: '100%',
  width: '100%',
  display: 'block',
  backgroundImage: `url(${"https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"})`,
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  border: 0,
  borderRadius: 5,
  boxShadow: '0 3px 5px 2px rgba(0, 0, 0, 0.54)',
  

},
blueCard:{
  'margin-bottom': '12%',
  background: '#111160',
  padding: '1% 2% 2% ',
  borderRadius: 5,
  display: 'block'
},
 header:{
  'text-transform': 'uppercase',
  padding: '0% 35% 10% '
 },
 
};



const App = props => {
  const { classes } = props;
 
  return (
    <Container maxWidth="lg" className={classes.root}>
      <Grid container alignitems="center" className={classes.header}>
      <Header />
      </Grid>
      <Grid  className= {classes.paperContainer}>
        <Grid 
         
        container 
        direction="row"
        justify="space-evenly">
          <ProjectList/>
        </Grid>
      </Grid>
    </Container>
  );
 };
 
 App.propTypes = {
  classes: PropTypes.object.isRequired,
 };
 
 export default withStyles(styles)(App);
